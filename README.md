# Introduction: #
This is the project associated with the QA Automation tests designed for the Videoclip Service.

-----

### Files distribution: ###
- Over folder *features/steps* are the Python files where the tests are described as the BDD description.
- Over folder *reports* are described the automation reports. This will be described in the next description.
- Over folder *files_needed* are the elements required to complete this tests.

----

### Libraries required in Python: ###
- behave
- requests
- time
- vlc

---

# How to run: #

- Step on project folder:

`cd Automation_Videoclips`
- Generate automated tests:

`behave -f allure_behave.formatter:AllureFormatter -o reports/ features/`

After success (or not) the automated tests, reports will be created in folder indicated.
- Generate reports based on the report files:

`allure serve reports/`

Allows us an .html file where reports are described, based on a UI full of options and settings.

----

### Tags, examples and complements: ###

- To perform a special feature:

`behave features/initSession_endSession.feature`
- Prints are not showed during automated tests. To allow them, use the following tags:

`behave --no-capture features/initSession_endSession.feature`

- If required, specific Scenarios can be performed individually over the next command:

`behave --no-capture features/initSession_endSession.feature --tags @Scenarios_to_perform`

----

### Manual Testing Functions: ###

1) check_m3u8_length.py:

Case of use: to check length of m3u8 videoClips contained

Example of use: by command line

`python check_m3u8_length.py <cameraMac> <startDate>`

`python check_m3u8_length.py AA:BB:CC:DD:EE:FE 1625260819000`

Output:

By terminal, it indicates videoClips length associated and if there is an error or not.
