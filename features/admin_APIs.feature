# To run this, we have to write :
# behave features/admin_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: X minutes X seconds
@every_scenario
Feature: Admin APIs

  @automation @finished @happy
  Scenario: Display metadata as admin - happy path
    Given Happy path -  getMetadataBetweenDateRange - info OK
    Then  Happy path -  getMetadataBetweenDateRange - Check status

  @automation @finished @happy @rainy
  Scenario: Display metadata as admin - blank elements
    Given Blank elements - getMetadataBetweenDateRange - rageStartDate
    And   Blank elements - getMetadataBetweenDateRange - rangeEndDate
    And   Blank elements - getMetadataBetweenDateRange - page
    And   Blank elements - getMetadataBetweenDateRange - size
    And   Blank elements - getMetadataBetweenDateRange - sort
    Then  Blank elements - getMetadataBetweenDateRange - Check status

  @automation @finished @happy @rainy
  Scenario: Display metadata as admin - wrong elements
    Given Wrong elements - getMetadataBetweenDateRange - rageStartDate
    And   Wrong elements - getMetadataBetweenDateRange - rangeEndDate
    And   Wrong elements - getMetadataBetweenDateRange - page
    And   Wrong elements - getMetadataBetweenDateRange - size
    And   Wrong elements - getMetadataBetweenDateRange - sort
    Then  Wrong elements - getMetadataBetweenDateRange - Check status
