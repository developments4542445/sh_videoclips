# Perform tests over the videoclips-customers.feature

from behave import *
from my_functions_class import *

"""
    This automation is designed based in previous clip was uploaded with the follow metadata:\n"
      "cameraMac: AA:BB:CC:DD:EE:56\n"
      "startDate: 1628532057000\n"
      "index: 0\n"
      "tokenId: 0\n"
"""


@given(u'Happy path - get videoClip as customer - info OK')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_video_clip(camera_mac='AA:BB:CC:DD:EE:56', start_date='1628532057000', index='0')

    test_finished()


@then(u'Happy path - get videoClip as customer - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 200 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)

    test_finished()


@given(u'Blank components - get videoClip as customer - cameraMac')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_video_clip(start_date='1628532057000', index='0')

    test_finished()


@given(u'Blank components - get videoClip as customer - startDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()
    context.my_video_clip_2.get_video_clip(camera_mac='AA:BB:CC:DD:EE:56', index='0')

    test_finished()


@given(u'Blank components - get videoClip as customer - index')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()
    context.my_video_clip_3.get_video_clip(camera_mac='AA:BB:CC:DD:EE:56', start_date='1628532057000')

    test_finished()


@then(u'Blank components - get videoClip as customer - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 404 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    assert 404 == context.my_video_clip_2.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    assert 404 == context.my_video_clip_3.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)

    test_finished()


@given(u'Wrong data - get videoClip as customer - cameraMac')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_video_clip(camera_mac='00:00:00:00:00:0G', start_date='1628532057000',
                                           index='0')

    test_finished()


@given(u'Wrong data - get videoClip as customer - startDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()
    context.my_video_clip_2.get_video_clip(camera_mac='AA:BB:CC:DD:EE:56', start_date='wrong_time',
                                           index='0')

    test_finished()


@given(u'Wrong data - get videoClip as customer - index')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()
    context.my_video_clip_3.get_video_clip(camera_mac='AA:BB:CC:DD:EE:56', start_date='1628532057000',
                                           index='wrong_index')

    test_finished()


@then(u'Wrong data - get videoClip as customer - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 400 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    assert 400 == context.my_video_clip_3.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)

    test_finished()
