# my_functions_class.py

import requests
import json
from requests.auth import HTTPBasicAuth


class CameraControlService:
    req = 'request'  # for json elements

    def __init__(self, mac='AA:BB:CC:DD:EE:53', environment='ist', username='mirgor', password='wayna!'):
        self.mac = mac
        self.environment = environment
        self.username = username
        self.password = password

    def set_command(self, element, command):
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setCommand',
                                 json={'argument': element, 'cameraMac': self.mac, 'command': command},
                                 auth=HTTPBasicAuth(self.username, self.password))

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def set_config(self, attribute, value):
        print()
        self.req = requests.post('https://' + self.environment + '-vs.mirgor.com.ar/cameracontrol/setConfig',
                                 json={'attributes': [{'attribute': attribute, 'value': value}],
                                       'cameraMac': self.mac},
                                 auth=HTTPBasicAuth(self.username, self.password))
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))


def test_finished():
    print()
    print('Last print do not work properly')
    pass


class VideoClipsServices:
    req = 'request'  # for json elements

    def __init__(self, mac='AA:BB:CC:DD:EE:99', environment='ist', username='mirgor', password='wayna!'):
        self.mac = mac
        self.environment = environment
        self.username = username
        self.password = password

    def upload_video_clip(self, camera_mac='blank_code', clip_duration='blank_code', end_date='blank_code',
                          event='blank_code', event_date='blank_code', index='blank_code',
                          segment_duration='blank_code', start_date='blank_code'):
        # 'https://ist-vs.mirgor.com.ar:443/videoclips/camera'

        url_to_send = 'https://' + self.environment + '-vs.mirgor.com.ar/videoclips/camera'
        json_element = {'cameraMac': camera_mac, 'clipDuration': clip_duration, 'endDate': end_date,
                        'event': event, 'eventDate': event_date, 'index': index, 'segmentDuration': segment_duration,
                        'startDate': start_date}

        file_path = '/home/fcapdeville/PycharmProjects/Automation_Videoclips/files_needed/example_videoclip.ts'
        files = open(file_path, 'rb')

        print()
        print('url to send: ' + url_to_send)

        headers = {'X-Auth-Token': 'Zafuuna2meeJ5shiheeg'}
        self.req = requests.post(url=url_to_send, files={'request': (None, json.dumps(json_element),
                                                                     'application/json'),
                                                         'file': files}, headers=headers)

        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def get_metadata_by_mac_and_date_range(self, camera_mac='blank_code', start_date='blank_code',
                                           end_date='blank_code', page='blank_code',
                                           size='blank_code', sort='blank_code'):
        # 'https://ist-vs.mirgor.com.ar/videoclips/metadata/'
        #                                 'mac?cameraMac=AA:BB:CC:DD:EE:99&rangeStartDate=26-09-2021 08:23:28'
        #                                 '&page=0&size=2&sort=startDate,desc'

        url_to_send = 'https://' + self.environment + '-vs.mirgor.com.ar/videoclips/metadata/mac?'
        if camera_mac != 'blank_code':
            url_to_send += 'cameraMac=' + camera_mac
        if start_date != 'blank_code':
            url_to_send += '&rangeStartDate=' + start_date
        if end_date != 'blank_code':
            url_to_send += '&rangeEndDate=' + end_date
        if page != 'blank_code':
            url_to_send += '&page=' + page
        if size != 'blank_code':
            url_to_send += '&size=' + size
        if sort != 'blank_code':
            url_to_send += '&sort=' + sort
        print()
        print('url to send: ' + url_to_send)

        headers = {'X-Auth-Token': 'Zafuuna2meeJ5shiheeg'}
        self.req = requests.get(url=url_to_send, headers=headers)
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

    def get_video_clip(self, camera_mac='blank_code', start_date='blank_code',
                       index='blank_code'):
        # 'https://ist-vs.mirgor.com.ar:443/videoclips/AA:BB:CC:DD:EE:FE/1625260819000/0'

        url_to_send = 'https://' + self.environment + '-vs.mirgor.com.ar/videoclips/'
        if camera_mac != 'blank_code':
            url_to_send += camera_mac + '/'
        if start_date != 'blank_code':
            url_to_send += start_date + '/'
        if index != 'blank_code':
            url_to_send += index + '/'
        print()
        print('url to send: ' + url_to_send)

        headers = {'X-Auth-Token': 'Zafuuna2meeJ5shiheeg'}
        self.req = requests.get(url=url_to_send, headers=headers)
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))

    def get_playlist(self, camera_mac='blank_code', start_date='blank_code'):
        # 'https://ist-vs.mirgor.com.ar:443/videoclips/playlist/AA:BB:CC:DD:EE:FE/1625260819000'

        url_to_send = 'https://' + self.environment + '-vs.mirgor.com.ar/videoclips/playlist/'
        if camera_mac != 'blank_code':
            url_to_send += camera_mac + '/'
        if start_date != 'blank_code':
            url_to_send += start_date + '/'
        print()
        print('url to send: ' + url_to_send)

        headers = {'X-Auth-Token': 'Zafuuna2meeJ5shiheeg'}
        self.req = requests.get(url=url_to_send, headers=headers)
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))

    def get_metadata_between_date_range(self, start_date='blank_code',
                                        end_date='blank_code', page='blank_code',
                                        size='blank_code', sort='blank_code'):
        # 'https://ist-vs.mirgor.com.ar/videoclips/admin/metadata'
        #                                 'rangeStartDate=08-06-2021 08:23:28&page=0&size=20&sort=startDate,asc'

        url_to_send = 'https://' + self.environment + '-vs.mirgor.com.ar/videoclips/admin/metadata?'
        if start_date != 'blank_code':
            url_to_send += 'rangeStartDate=' + start_date
        if end_date != 'blank_code':
            url_to_send += '&rangeEndDate=' + end_date
        if page != 'blank_code':
            url_to_send += '&page=' + page
        if size != 'blank_code':
            url_to_send += '&size=' + size
        if sort != 'blank_code':
            url_to_send += '&sort=' + sort
        print()
        print('url to send: ' + url_to_send)

        headers = {'X-Auth-Token': 'Zafuuna2meeJ5shiheeg'}
        self.req = requests.get(url=url_to_send, headers=headers)
        # Checking by HTTP Response
        print("Status: " + str(self.req.status_code))
        print("Content: " + str(self.req.content))

