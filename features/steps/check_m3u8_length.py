# Perform tests over the videoclips-customers.feature

from my_functions_class import *
from termcolor import colored
import sys

# Example of writing over terminal:
# python check_m3u8_length.py AA:BB:CC:DD:EE:FE 1625260819000

if __name__ == '__main__':
    my_video_clip_1 = VideoClipsServices()
    my_video_clip_1.get_playlist(camera_mac=sys.argv[1], start_date=sys.argv[2])

    print("Content: " + str(my_video_clip_1.req.content))
    print()

    # Asserts - status code
    assert 200 == my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                   str(my_video_clip_1.req.status_code) + \
                                                   '. content: ' + str(my_video_clip_1.req.content)

    # Parse string and getting every line separately
    string_to_test = str(my_video_clip_1.req.content).split('\\')

    counter = 0
    for counter in range(0, len(string_to_test)):
        if string_to_test[counter].startswith('n#EXTINF:'):                 # Obtain elements required
            extension_value = string_to_test[counter].split(':')
            file_length = extension_value[1][:-1]                           # Obtain numeric values i.e.: '15.0'
            file_length = int(file_length.replace('.', ''))                 # Removing point i.e.: '150'

            if 0 < file_length <= 150:
                print(colored('Extension of files in this .m3u8: ' + str(file_length), 'green'))
            else:
                print(colored('ERROR: Found in the file: ' + str(file_length), 'red'))
