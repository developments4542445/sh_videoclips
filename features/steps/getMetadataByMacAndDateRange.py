# Perform tests over the videoclips-customers.feature

from behave import *
from my_functions_class import *

"""
    This automation is designed based in previous clip was uploaded with the follow metadata:\n"
      "cameraMac: AA:BB:CC:DD:EE:56\n"
      "startDate: 09-08-2021 15:00:57\n"
      "eventDate: 09-08-2021 15:01:40\n"
      "endDate: 09-08-2021 15:01:50\n"
      "event: sound\n"
      "playlistUrl: /playlist/AA:BB:CC:DD:EE:56/1628532057000\n"
"""


@given(u'Happy path -  getMetadataByMacAndDateRange - info OK')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_metadata_by_mac_and_date_range('AA:BB:CC:DD:EE:56', '08-06-2021 08:23:28',
                                                               '29-09-2021 08:23:28', '0', '2', 'startDate,desc')

    test_finished()


@then(u'Happy path -  getMetadataByMacAndDateRange - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 200 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)

    # Get the first element of the json
    element_to_check = context.my_video_clip_1.req.json()['content'][0]

    # Asserts - content
    assert 'AA:BB:CC:DD:EE:56' == str(element_to_check['cameraMac']), \
        "Difference in cameraMac: " + str(element_to_check['cameraMac'])
    assert '09-08-2021 15:00:57' == str(element_to_check['startDate']),\
        "Difference in startDate: " + str(element_to_check['startDate'])
    assert '09-08-2021 15:01:40' == str(element_to_check['eventDate']), \
        "Difference in eventDate: " + str(element_to_check['eventDate'])
    assert '09-08-2021 15:01:50' == str(element_to_check['endDate']), \
        "Difference in endDate: " + str(element_to_check['endDate'])
    assert 'motion' == str(element_to_check['event']), \
        "Difference in event: " + str(element_to_check['event'])
    assert '/playlist/AA:BB:CC:DD:EE:56/1628532057000' == str(element_to_check['playlistUrl']), \
        "Difference in playlistUrl: " + str(element_to_check['playlistUrl'])

    test_finished()


@given(u'Blank elements - getMetadataByMacAndDateRange - cameraMac')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_metadata_by_mac_and_date_range(start_date='09-08-2021 15:00:57',
                                                               end_date='09-08-2021 15:01:40', page='0',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Blank elements - getMetadataByMacAndDateRange - rangeStartDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()
    context.my_video_clip_2.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               end_date='09-08-2021 15:01:40', page='0',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Blank elements - getMetadataByMacAndDateRange - rangeEndDate')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()
    context.my_video_clip_3.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='09-08-2021 15:00:57', page='0',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Blank elements - getMetadataByMacAndDateRange - page')
def step_impl(context):
    print()
    context.my_video_clip_4 = VideoClipsServices()
    context.my_video_clip_4.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='09-08-2021 15:00:57',
                                                               end_date='09-08-2021 15:01:40',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Blank elements - getMetadataByMacAndDateRange - size')
def step_impl(context):
    print()
    context.my_video_clip_5 = VideoClipsServices()
    context.my_video_clip_5.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='09-08-2021 15:00:57',
                                                               end_date='09-08-2021 15:01:40',
                                                               page='0', sort='startDate,desc')

    test_finished()


@given(u'Blank elements - getMetadataByMacAndDateRange - sort')
def step_impl(context):
    print()
    context.my_video_clip_6 = VideoClipsServices()
    context.my_video_clip_6.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='09-08-2021 15:00:57',
                                                               end_date='09-08-2021 15:01:40', page='0', size='2')

    test_finished()


@then(u'Blank elements - getMetadataByMacAndDateRange - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 400 == context.my_video_clip_1.req.status_code, "ERROR: cameraMac - status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR: rangeStartDate - status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    assert 200 == context.my_video_clip_3.req.status_code, "ERROR: rangeEndDate - status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)
    assert 200 == context.my_video_clip_4.req.status_code, "ERROR: page - status_code: " + \
                                                           str(context.my_video_clip_4.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_4.req.content)
    assert 200 == context.my_video_clip_5.req.status_code, "ERROR: size - status_code: " + \
                                                           str(context.my_video_clip_5.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_5.req.content)
    assert 200 == context.my_video_clip_6.req.status_code, "ERROR: sort - status_code: " + \
                                                           str(context.my_video_clip_6.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_6.req.content)

    test_finished()


@given(u'Wrong elements - getMetadataByMacAndDateRange - cameraMac')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_metadata_by_mac_and_date_range(camera_mac='00:00:00:00:00:0G',
                                                               start_date='08-06-2021 08:23:28',
                                                               end_date='26-09-2021 08:23:28', page='0',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Wrong elements - getMetadataByMacAndDateRange - rangeStartDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()
    context.my_video_clip_2.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='26-15-2021 08:23:28',
                                                               end_date='26-09-2021 08:23:28', page='0',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Wrong elements - getMetadataByMacAndDateRange - rangeEndDate')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()
    context.my_video_clip_3.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='08-06-2021 08:23:28',
                                                               end_date='26-15-2021 08:23:28', page='0',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Wrong elements - getMetadataByMacAndDateRange - page')
def step_impl(context):
    print()
    context.my_video_clip_4 = VideoClipsServices()
    context.my_video_clip_4.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='08-06-2021 08:23:28',
                                                               end_date='26-09-2021 08:23:28', page='test',
                                                               size='2', sort='startDate,desc')

    test_finished()


@given(u'Wrong elements - getMetadataByMacAndDateRange - size')
def step_impl(context):
    print()
    context.my_video_clip_5 = VideoClipsServices()
    context.my_video_clip_5.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='08-06-2021 08:23:28',
                                                               end_date='26-09-2021 08:23:28', page='0',
                                                               size='test', sort='startDate,desc')

    test_finished()


@given(u'Wrong elements - getMetadataByMacAndDateRange - sort')
def step_impl(context):
    print()
    context.my_video_clip_6 = VideoClipsServices()
    context.my_video_clip_6.get_metadata_by_mac_and_date_range(camera_mac='AA:BB:CC:DD:EE:56',
                                                               start_date='08-06-2021 08:23:28',
                                                               end_date='26-09-2021 08:23:28', page='0',
                                                               size='2', sort='test')

    test_finished()


@then(u'Wrong elements - getMetadataByMacAndDateRange - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 400 == context.my_video_clip_1.req.status_code, "ERROR: cameraMac - status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR: rangeStartDate - status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    assert 400 == context.my_video_clip_3.req.status_code, "ERROR: rangeEndDate - status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)
    assert 200 == context.my_video_clip_4.req.status_code, "ERROR: page - status_code: " + \
                                                           str(context.my_video_clip_4.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_4.req.content)
    assert 200 == context.my_video_clip_5.req.status_code, "ERROR: size - status_code: " + \
                                                           str(context.my_video_clip_5.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_5.req.content)
    assert 400 == context.my_video_clip_6.req.status_code, "ERROR: sort - status_code: " + \
                                                           str(context.my_video_clip_6.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_6.req.content)

    test_finished()
