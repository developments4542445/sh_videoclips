# Perform tests over the videoclips-camera.feature

from behave import *
from my_functions_class import *
import time


@given(u'Perform uploadVideoClip - info OK')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()

    time_epoch = int(time.time())
    start_date = '' + str(time_epoch) + '000'
    event_date = '' + str(time_epoch) + '000'
    end_date = '' + str(time_epoch + 1000) + '000'

    context.my_video_clip_1.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=end_date, event='sound', event_date=event_date,
                                              index='0', segment_duration='16.2', start_date=start_date)

    test_finished()


@then(u'Upload videoClip as device - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 201 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)

    test_finished()


@given(u'Upload a first videoClip - info OK')
def step_impl(context):
    print()

    time.sleep(2)
    context.my_video_clip_1 = VideoClipsServices()

    time_epoch = int(time.time())
    context.start_date = '' + str(time_epoch) + '000'
    context.event_date = '' + str(time_epoch) + '000'
    context.end_date = '' + str(time_epoch + 1000) + '000'

    context.my_video_clip_1.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Upload a duplicated videoClip - same info')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()

    context.my_video_clip_2.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@then(u'Upload duplicated videoClip - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 201 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)

    test_finished()


@given(u'Invalid dates - uploadVideoClip - info OK')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()

    time_epoch = int(time.time())
    context.first_date = '' + str(time_epoch) + '000'
    context.second_date = '' + str(time_epoch) + '000'
    context.third_date = '' + str(time_epoch + 1000) + '000'

    context.my_video_clip_1.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.third_date, event='sound',
                                              event_date=context.second_date,
                                              index='0', segment_duration='16.2', start_date=context.first_date)

    test_finished()


@given(u'Invalid dates - startDate < endDate   < eventDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()

    context.my_video_clip_2.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.second_date, event='sound',
                                              event_date=context.third_date,
                                              index='0', segment_duration='16.2', start_date=context.first_date)

    test_finished()


@given(u'Invalid dates - eventDate < startDate < endDate')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()

    context.my_video_clip_3.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.third_date, event='sound',
                                              event_date=context.first_date,
                                              index='0', segment_duration='16.2', start_date=context.second_date)

    test_finished()


@given(u'Invalid dates - eventDate < endDate   < startDate')
def step_impl(context):
    print()
    context.my_video_clip_4 = VideoClipsServices()

    context.my_video_clip_4.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.second_date, event='sound',
                                              event_date=context.first_date,
                                              index='0', segment_duration='16.2', start_date=context.third_date)

    test_finished()


@given(u'Invalid dates - endDate   < startDate < eventDate')
def step_impl(context):
    print()
    context.my_video_clip_5 = VideoClipsServices()

    context.my_video_clip_5.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.first_date, event='sound',
                                              event_date=context.third_date,
                                              index='0', segment_duration='16.2', start_date=context.second_date)

    test_finished()


@given(u'Invalid dates - endDate   < eventDate < startDate')
def step_impl(context):
    print()
    context.my_video_clip_6 = VideoClipsServices()

    context.my_video_clip_6.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.first_date, event='sound',
                                              event_date=context.second_date,
                                              index='0', segment_duration='16.2', start_date=context.third_date)

    test_finished()


@then(u'Invalid dates - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 201 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR: startDate < endDate < eventDate - status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_3.req.status_code, "ERROR: eventDate < startDate < endDate - status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_4.req.status_code, "ERROR: eventDate < endDate < startDate - status_code: " + \
                                                           str(context.my_video_clip_4.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_4.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_5.req.status_code, "ERROR: endDate < startDate < eventDate - status_code: " + \
                                                           str(context.my_video_clip_5.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_5.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_6.req.status_code, "ERROR: endDate < eventDate < startDate - status_code: " + \
                                                           str(context.my_video_clip_6.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_6.req.content)

    test_finished()


@given(u'Blank components - uploadVideoClip - cameraMac')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()

    time_epoch = int(time.time())
    context.start_date = '' + str(time_epoch) + '000'
    context.event_date = '' + str(time_epoch) + '000'
    context.end_date = '' + str(time_epoch + 1000) + '000'

    context.my_video_clip_1.upload_video_clip(clip_duration='16.2',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - clipDuration')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()

    context.my_video_clip_2.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - endDate')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()

    context.my_video_clip_3.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              event='sound', event_date=context.event_date,
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - event')
def step_impl(context):
    print()
    context.my_video_clip_4 = VideoClipsServices()

    context.my_video_clip_4.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event_date=context.event_date,
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - eventDate')
def step_impl(context):
    print()
    context.my_video_clip_5 = VideoClipsServices()

    context.my_video_clip_5.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event='sound',
                                              index='0', segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - index')
def step_impl(context):
    print()
    context.my_video_clip_6 = VideoClipsServices()

    context.my_video_clip_6.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              segment_duration='16.2', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - segmentDuration')
def step_impl(context):
    print()
    context.my_video_clip_7 = VideoClipsServices()

    context.my_video_clip_7.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              index='0', start_date=context.start_date)

    test_finished()


@given(u'Blank components - uploadVideoClip - startDate')
def step_impl(context):
    print()
    context.my_video_clip_8 = VideoClipsServices()

    context.my_video_clip_8.upload_video_clip(camera_mac='AA:BB:CC:DD:EE:99', clip_duration='16.2',
                                              end_date=context.end_date, event='sound', event_date=context.event_date,
                                              index='0', segment_duration='16.2')

    test_finished()


@then(u'Blank components - uploadVideoClip - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 400 == context.my_video_clip_1.req.status_code, "ERROR: uploadVideoClip cameraMac -  status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR: uploadVideoClip clipDuration - status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_3.req.status_code, "ERROR: uploadVideoClip endDate - status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_4.req.status_code, "ERROR: uploadVideoClip event - status_code: " + \
                                                           str(context.my_video_clip_4.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_4.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_5.req.status_code, "ERROR: uploadVideoClip eventDate - status_code: " + \
                                                           str(context.my_video_clip_5.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_5.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_6.req.status_code, "ERROR: uploadVideoClip index - status_code: " + \
                                                           str(context.my_video_clip_6.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_6.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_7.req.status_code, "ERROR: uploadVideoClip segmentDuration - status_code: " + \
                                                           str(context.my_video_clip_7.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_7.req.content)
    # Asserts - status code
    assert 400 == context.my_video_clip_8.req.status_code, "ERROR: uploadVideoClip startDate - status_code: " + \
                                                           str(context.my_video_clip_8.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_8.req.content)

    test_finished()
