# Perform tests over the videoclips-admin.feature

from behave import *
from my_functions_class import *

"""
    This automation is designed based in previous clip was uploaded with the follow metadata:\n"
      "cameraMac: AA:BB:CC:DD:EE:FE\n"
      "startDate: 1627994264000\n"
      "index: 0\n"
      "tokenId: 0\n"
"""


@given(u'Happy path -  getMetadataBetweenDateRange - info OK')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_metadata_between_date_range('08-06-2021 08:23:28', '29-09-2021 08:23:28',
                                                            '0', '2', 'startDate,asc')

    test_finished()


@then(u'Happy path -  getMetadataBetweenDateRange - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 200 == context.my_video_clip_1.req.status_code, "ERROR. status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)

    # Get the first element of the json
    element_to_check = context.my_video_clip_1.req.json()['content'][0]

    # Asserts - content
    assert '03-08-2021 09:37:44' == str(element_to_check['startDate']),\
        "Difference in startDate: " + str(element_to_check['startDate'])
    assert '03-08-2021 09:37:45' == str(element_to_check['eventDate']), \
        "Difference in eventDate: " + str(element_to_check['eventDate'])
    assert '03-08-2021 09:38:44' == str(element_to_check['endDate']), \
        "Difference in endDate: " + str(element_to_check['endDate'])
    assert 'motion' == str(element_to_check['event']), \
        "Difference in event: " + str(element_to_check['event'])
    assert '/playlist/AA:BB:CC:DD:EE:FE/1627994264000' == str(element_to_check['playlistUrl']), \
        "Difference in playlistUrl: " + str(element_to_check['playlistUrl'])

    test_finished()


@given(u'Blank elements - getMetadataBetweenDateRange - rageStartDate')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_metadata_between_date_range(end_date='03-08-2021 09:38:44', page='0',
                                                            size='2', sort='startDate,asc')

    test_finished()


@given(u'Blank elements - getMetadataBetweenDateRange - rangeEndDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()
    context.my_video_clip_2.get_metadata_between_date_range(start_date='03-08-2021 09:37:44', page='0',
                                                            size='2', sort='startDate,asc')

    test_finished()


@given(u'Blank elements - getMetadataBetweenDateRange - page')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()
    context.my_video_clip_3.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44',
                                                            size='2', sort='startDate,asc')

    test_finished()


@given(u'Blank elements - getMetadataBetweenDateRange - size')
def step_impl(context):
    print()
    context.my_video_clip_4 = VideoClipsServices()
    context.my_video_clip_4.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44', page='0',
                                                            sort='startDate,asc')

    test_finished()


@given(u'Blank elements - getMetadataBetweenDateRange - sort')
def step_impl(context):
    print()
    context.my_video_clip_5 = VideoClipsServices()
    context.my_video_clip_5.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44', page='0',
                                                            size='2')

    test_finished()


@then(u'Blank elements - getMetadataBetweenDateRange - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 400 == context.my_video_clip_1.req.status_code, "ERROR: rageStartDate - status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    assert 200 == context.my_video_clip_2.req.status_code, "ERROR: rangeEndDate - status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    assert 200 == context.my_video_clip_3.req.status_code, "ERROR: page - status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)
    assert 200 == context.my_video_clip_4.req.status_code, "ERROR: size - status_code: " + \
                                                           str(context.my_video_clip_4.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_4.req.content)
    assert 200 == context.my_video_clip_5.req.status_code, "ERROR: sort - status_code: " + \
                                                           str(context.my_video_clip_5.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_5.req.content)

    test_finished()


@given(u'Wrong elements - getMetadataBetweenDateRange - rageStartDate')
def step_impl(context):
    print()
    context.my_video_clip_1 = VideoClipsServices()
    context.my_video_clip_1.get_metadata_between_date_range(start_date='03-15-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44', page='0',
                                                            size='2', sort='startDate,asc')

    test_finished()


@given(u'Wrong elements - getMetadataBetweenDateRange - rangeEndDate')
def step_impl(context):
    print()
    context.my_video_clip_2 = VideoClipsServices()
    context.my_video_clip_2.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-15-2021 09:38:44', page='0',
                                                            size='2', sort='startDate,asc')

    test_finished()


@given(u'Wrong elements - getMetadataBetweenDateRange - page')
def step_impl(context):
    print()
    context.my_video_clip_3 = VideoClipsServices()
    context.my_video_clip_3.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44', page='test',
                                                            size='2', sort='startDate,asc')

    test_finished()


@given(u'Wrong elements - getMetadataBetweenDateRange - size')
def step_impl(context):
    print()
    context.my_video_clip_4 = VideoClipsServices()
    context.my_video_clip_4.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44', page='0',
                                                            size='test', sort='startDate,asc')

    test_finished()


@given(u'Wrong elements - getMetadataBetweenDateRange - sort')
def step_impl(context):
    print()
    context.my_video_clip_5 = VideoClipsServices()
    context.my_video_clip_5.get_metadata_between_date_range(start_date='03-08-2021 09:37:44',
                                                            end_date='03-08-2021 09:38:44', page='0',
                                                            size='2', sort='test')

    test_finished()


@then(u'Wrong elements - getMetadataBetweenDateRange - Check status')
def step_impl(context):
    print()

    # Asserts - status code
    assert 400 == context.my_video_clip_1.req.status_code, "ERROR: rangeStartDate - status_code: " + \
                                                           str(context.my_video_clip_1.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_1.req.content)
    assert 400 == context.my_video_clip_2.req.status_code, "ERROR: rangeEndDate - status_code: " + \
                                                           str(context.my_video_clip_2.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_2.req.content)
    assert 200 == context.my_video_clip_3.req.status_code, "ERROR: page - status_code: " + \
                                                           str(context.my_video_clip_3.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_3.req.content)
    assert 200 == context.my_video_clip_4.req.status_code, "ERROR: size - status_code: " + \
                                                           str(context.my_video_clip_4.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_4.req.content)
    assert 200 == context.my_video_clip_5.req.status_code, "ERROR: sort - status_code: " + \
                                                           str(context.my_video_clip_5.req.status_code) + \
                                                           '. content: ' + str(context.my_video_clip_5.req.content)

    test_finished()
