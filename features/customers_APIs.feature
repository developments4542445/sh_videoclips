# To run this, we have to write :
# behave features/customers_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: X minutes X seconds
@every_scenario
Feature: Customers APIs

  @automation @finished @happy
  Scenario: Display metadata as customer - happy path
    Given Happy path -  getMetadataByMacAndDateRange - info OK
    Then  Happy path -  getMetadataByMacAndDateRange - Check status

  @automation @finished @happy @rainy
  Scenario: Display metadata as customer - blank elements
    Given Blank elements - getMetadataByMacAndDateRange - cameraMac
    And   Blank elements - getMetadataByMacAndDateRange - rangeStartDate
    And   Blank elements - getMetadataByMacAndDateRange - rangeEndDate
    And   Blank elements - getMetadataByMacAndDateRange - page
    And   Blank elements - getMetadataByMacAndDateRange - size
    And   Blank elements - getMetadataByMacAndDateRange - sort
    Then  Blank elements - getMetadataByMacAndDateRange - Check status

  @automation @finished @happy @rainy
  Scenario: Display metadata as customer - wrong elements
    Given Wrong elements - getMetadataByMacAndDateRange - cameraMac
    And   Wrong elements - getMetadataByMacAndDateRange - rangeStartDate
    And   Wrong elements - getMetadataByMacAndDateRange - rangeEndDate
    And   Wrong elements - getMetadataByMacAndDateRange - page
    And   Wrong elements - getMetadataByMacAndDateRange - size
    And   Wrong elements - getMetadataByMacAndDateRange - sort
    Then  Wrong elements - getMetadataByMacAndDateRange - Check status

  @automation @finished @happy
  Scenario: Get videoClip as customer - happy path
    Given Happy path - get videoClip as customer - info OK
    Then  Happy path - get videoClip as customer - Check status

  @automation @finished @happy @rainy
  Scenario: Get videoClip as customer - blank components
    Given Blank components - get videoClip as customer - cameraMac
    And   Blank components - get videoClip as customer - startDate
    And   Blank components - get videoClip as customer - index
    Then  Blank components - get videoClip as customer - Check status

  @automation @finished @happy @rainy
  Scenario: Download videoClip with wrong data
    Given Wrong data - get videoClip as customer - cameraMac
    And   Wrong data - get videoClip as customer - startDate
    And   Wrong data - get videoClip as customer - index
    Then  Wrong data - get videoClip as customer - Check status

  @automation @finished @happy
  Scenario: Download videoClips as a playlist - .m3u8 file
    Given Happy path - download videoClips as a playlist - info OK
    Then  Happy path - download videoClips as a playlist - Check status

  @automation @finished @rainy
  Scenario: Download videoClips as a playlist - blank components
    Given Blank components - download videoClips as a playlist - cameraMac
    And   Blank components - download videoClips as a playlist - startDate
    Then  Blank components - download videoClips as a playlist - Check status

  @automation @finished @rainy
  Scenario: Download videoClips as a playlist - wrong components
    Given Wrong components - download videoClips as a playlist - cameraMac
    And   Wrong components - download videoClips as a playlist - startDate
    Then  Wrong components - download videoClips as a playlist - Check status