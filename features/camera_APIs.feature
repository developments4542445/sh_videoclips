# To run this, we have to write :
# behave features/camera_APIs.feature
# in the terminal
# if we want to run every test in the folder, we use
# behave ./features

# Took: X minutes X seconds
@every_scenario
Feature: Camera APIs

  @automation @finished @happy
  Scenario: Upload videoClip as device
    Given Perform uploadVideoClip - info OK
    Then Upload videoClip as device - Check status

  @automation @finished @rainy
  Scenario: Upload duplicated videoClip
    Given Upload a first videoClip - info OK
    And   Upload a duplicated videoClip - same info
    Then  Upload duplicated videoClip - Check status

  @automation @finished @rainy
  Scenario: Upload videoClip with invalid dates
    Given Invalid dates - uploadVideoClip - info OK
    And   Invalid dates - startDate < endDate   < eventDate
    And   Invalid dates - eventDate < startDate < endDate
    And   Invalid dates - eventDate < endDate   < startDate
    And   Invalid dates - endDate   < startDate < eventDate
    And   Invalid dates - endDate   < eventDate < startDate
    Then  Invalid dates - Check status

  @automation @finished @rainy
  Scenario: Upload videoClip as device - blank components
    Given Blank components - uploadVideoClip - cameraMac
    And   Blank components - uploadVideoClip - clipDuration
    And   Blank components - uploadVideoClip - endDate
    And   Blank components - uploadVideoClip - event
    And   Blank components - uploadVideoClip - eventDate
    And   Blank components - uploadVideoClip - index
    And   Blank components - uploadVideoClip - segmentDuration
    And   Blank components - uploadVideoClip - startDate
    Then  Blank components - uploadVideoClip - Check status
